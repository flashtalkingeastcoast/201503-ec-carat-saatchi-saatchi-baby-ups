package com.flashtalking.Saatchi{
	
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	
	public class WalkingBabies extends MovieClip{
		public var leftBaby:Toddler;
		public var rightBaby:MovieClip;
		public var theLeakCompleteCall:Function;
		public var leakTimer:Timer;
		
		public function WalkingBabies() {
			super();
			leftBaby.setToddlerDoneFunction(startLeak);
		}
		
		public function startLeak():void {
			trace("starting leak");
			//start timer
			leakTimer = new Timer(2000, 1);
			leakTimer.addEventListener(TimerEvent.TIMER_COMPLETE, leakComplete);
			leakTimer.start();
		}
		
		/*var timer:Timer = new Timer(1000, 2);
		timer.addEventListener(TimerEvent.TIMER, blah);
		timer.start();
		
		function blah(e:TimerEvent):void{
			trace("Times Fired: " + e.currentTarget.currentCount);
			trace("Time Delayed: " + e.currentTarget.delay);
		}*/
		
		public function setLeakCompleteCall(theFunction:Function):void {
			theLeakCompleteCall = theFunction;
		}
		
		public function leakComplete(e:TimerEvent=null):void {
			theLeakCompleteCall();
			TweenLite.to(leftBaby, .5, {alpha:0, delay:2});
			TweenLite.to(rightBaby, .5, {alpha:0, delay:2});
		}
		
		public function startToddlers():void {
			leftBaby.gotoAndPlay(2);
			rightBaby.gotoAndPlay(2);
		}
	}
}