package com.flashtalking.Saatchi{
	
	import flash.display.MovieClip;
	
	
	public class Toddler extends MovieClip{
		public var toddlerDoneCall:Function;
		
		public function Toddler(){
			super();
		}
		
		public function setToddlerDoneFunction(theFunction:Function):void {
			toddlerDoneCall = theFunction;	
		}
		
		public function toddlerDone():void {
			toddlerDoneCall();
		}
	}
}