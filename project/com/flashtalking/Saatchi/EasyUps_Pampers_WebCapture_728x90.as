package com.flashtalking.Saatchi{
	
	import com.flashtalking.Expand;
	import com.flashtalking.FT;
	import com.flashtalking.InstantAds;
	import com.flashtalking.RichloadPlaceholder;
	import com.flashtalking.RichloadSetup;
	import com.flashtalking.events.FTEvent;
	import com.flashtalking.util.DrawingHelper;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	
	
	public class EasyUps_Pampers_WebCapture_728x90 extends MovieClip{
		// Component Variables
		// We create an ad setup, an InstantAds & an ImagePlaceholder component which will display our instant ads image.
		public var myFT:FT = new FT();
		private var myInstantAds:InstantAds = new InstantAds();
		
		private var myRichloadSetup:RichloadSetup = new RichloadSetup();
		private var myRichloadPlaceholder:RichloadPlaceholder = new RichloadPlaceholder();
		private var theRichloadURL1:String;
		private var theTargetRichload1:String = RichloadSetup.RICHLOAD1;
		
		private var myRichloadSetup2:RichloadSetup = new RichloadSetup();
		private var myRichloadPlaceholder2:RichloadPlaceholder = new RichloadPlaceholder();
		private var theRichloadURL2:String;
		private var theTargetRichload2:String = RichloadSetup.RICHLOAD2;
		
		private var myExpand:Expand;
		
		//loaders for assets
		private var loadedItemsArray:Array = new Array();
		private var totalItemsToLoad:Number = 3;			//items to load: instant ads, richload 1, richload 2
		public var theBorder:Shape;
		
		
		public function EasyUps_Pampers_WebCapture_728x90() {
			
		}
		
		public function init(rl_1_name:String, rl_2_name:String):void {
			theRichloadURL1 = rl_1_name;
			theRichloadURL2 = rl_2_name;
			myFT.clickThruType = FT.NO_CLICK;
			//TODO
			myFT.numClickTags = 2;									//Need to check on this
			//adding the FT component
			addChild(myFT);
			//set default IA variables and add instant ad component
			create_IA_vars();
			addChild(myInstantAds);
			
			// Set the properties of the RichloadSetup Component.
			myRichloadSetup.richloadURL = theRichloadURL1;
			myRichloadSetup.targetRichload = theTargetRichload1;		
			myRichloadSetup2.richloadURL = theRichloadURL2;
			myRichloadSetup2.targetRichload = theTargetRichload2;
			
			addChild(myRichloadSetup);
			addChild(myRichloadSetup2);
			
			// Add a listener to wait for the instant ads to be loaded before adding the richload placeholder.
			// This is to make sure that the correct richload is loaded, and not always the default one.
			myFT.events.addEventListener(FTEvent.INSTANT_ADS_LOADED, initialiseInstantAdsVariables);	
			myFT.events.addEventListener(FTEvent.READY_TO_PLAY, richloadReady);
			
			myFT.events.addEventListener(FTEvent.AD_EXPAND, adExpanded);
			myFT.events.addEventListener(FTEvent.AD_CONTRACT, adContracted);
			
			myExpand = new Expand();
			myExpand.expandedWidth = 1100;
			myExpand.expandedHeight = 600;
			myExpand.unexpandedWidth = 728;
			myExpand.unexpandedHeight = 90;
			myExpand.expandTrigger = Expand.MANUAL;
			myExpand.contractTrigger = Expand.MANUAL;
			addChild(myExpand);
		}
		
		private function create_IA_vars():void {
			//creating the default IA variables
			myInstantAds.registeredInstantAds = [
				//	728x90 //////////////////////////////////////////////////////
				{name:"richloadVariable",				type:InstantAds.RICHLOAD,	value:"richload1"},
				{name:"richloadVariable",				type:InstantAds.RICHLOAD,	value:"richload2"},
				{name:"UserInitiated",					type:InstantAds.TEXT,		value:"false"}
						
			];
		}
		
		private function initialiseInstantAdsVariables(evt:FTEvent):void {
			
			loadedItemsArray.push(evt.type);
			
			myFT.events.removeEventListener(FTEvent.INSTANT_ADS_LOADED, initialiseInstantAdsVariables);	
			// Set the properties of the RichloadPlaceholder Component.
			myRichloadPlaceholder.targetRichload = RichloadPlaceholder.RICHLOAD1;
			addChild(myRichloadPlaceholder);
			myRichloadPlaceholder2.targetRichload = RichloadPlaceholder.RICHLOAD2;
			myRichloadPlaceholder2.x = 728;
			addChild(myRichloadPlaceholder2);
			
			/*var borderColor:String = "000000";
			theBorder = DrawingHelper.drawAdBorder(stage, DrawingHelper.convertToFlashHex(borderColor), 1, 1);
			addChild(theBorder);*/
			
			//loadFeed();
			trace("IA vars");
			checkIfReadyToLoadAssets();
		}
		
		private function richloadReady(evt:FTEvent):void{
			//trace("loaded event type: "+evt.type);
			loadedItemsArray.push(evt.type);
			//myFT.events.removeEventListener(FTEvent.READY_TO_PLAY, initialiseInstantAdsVariables);
			trace("rl ready");
			checkIfReadyToLoadAssets();
		}
		
		private function checkIfReadyToLoadAssets():void {
			trace("checkIfReadyToLoadAssets - loadedItemsArray.length: "+loadedItemsArray.length);
			if(loadedItemsArray.length == 3){
				myFT.events.removeEventListener(FTEvent.READY_TO_PLAY, richloadReady);
				//loadRemainingAssets();
				//open expand, start babies walking
				//myFT.api.richload1.comms.startAnimation();
				myFT.api.expand.expand();
				
			}
		}
		
		private function  adExpanded(evt:FTEvent):void{
			trace("event AD_EXPAND fired");
			myRichloadPlaceholder.x = 186;
			myRichloadPlaceholder2.x = 0;
			//addChild(myRichloadPlaceholder2);
			myFT.api.richload1.comms.startAnimation();
		}
		
		private function  adContracted(evt:FTEvent):void{
			trace("event AD_CONTRACT fired");
			//removeChild(myRichloadPlaceholder2);
			myRichloadPlaceholder.x = 0;
			myRichloadPlaceholder2.x = 728;
		} 
	}
}