package com.flashtalking.Saatchi{
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.filters.DisplacementMapFilter;
	import flash.filters.DisplacementMapFilterMode;
	
	
	public class PeeDisplacement{
		
		public var bm:BitmapData;
		public var disp:DisplacementMapFilter;
		public var offsets:Array;
		
		public var waterWidth:Number;
		public var waterHeight:Number;
		public var water:MovieClip;
		
		public function PeeDisplacement(theWaterWidth:Number, theWaterHeight:Number, theWater:MovieClip) {
			waterWidth = theWaterWidth;
			waterHeight = theWaterHeight;
			water = theWater;
			bm = new BitmapData(waterWidth, waterHeight);  
			
			disp = new DisplacementMapFilter(bm,new Point(10,10),40,2,10,15, DisplacementMapFilterMode.CLAMP);
			offsetse = [new Point(0, 0), new Point(30, 0)];  
		}
	}
	
	public function startPee(referenceMovieClip:MovieClip):void {
		referenceMovieClip.addEventListene(Event.ENTER_FRAME, doUpdate); 
	}
	
	public function stopPee(referenceMovieClip:MovieClip):void {
		referenceMovieClip.removeEventListene(Event.ENTER_FRAME, doUpdate); 
	}
	
	public function doUpdate(evt:Event):void   {  
		offsets[0].x -=.9;  
		offsets[1].y -=.5;  
		bm.perlinNoise(145, 120, 2 ,150, true, false, 7, true, offsets);  
		water.filters=[disp];  
	} 
}