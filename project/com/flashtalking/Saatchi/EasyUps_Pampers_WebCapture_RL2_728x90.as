package com.flashtalking.Saatchi{
	
	import com.flashtalking.util.CrossDomainCode;
	
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
	
	
	public class EasyUps_Pampers_WebCapture_RL2_728x90 extends CrossDomainCode{
		//This is the page takeover with the babies
		public var toddlers:WalkingBabies;
		public var theClickTag:SimpleButton;
		
		public function EasyUps_Pampers_WebCapture_RL2_728x90() {
			
		}
		
		public function init():void {
			theClickTag.addEventListener(MouseEvent.CLICK, doClickTag);
		}
		
		public function startAnimation():void {
			trace("starting babies to walk");
			toddlers.startToddlers();
			toddlers.setLeakCompleteCall(leakCompleteCall);
		}
		
		public function leakCompleteCall():void {
			myFT.api.richload1.comms.complete_rL1_initialSequence();
		}
		
		public function doClickTag(evt:MouseEvent):void {
			myFT.clickTag(2);
		}
	}
}