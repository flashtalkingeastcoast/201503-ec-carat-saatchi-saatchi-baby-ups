package com.flashtalking.Saatchi{
	
	import com.flashtalking.util.CrossDomainCode;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
	
	
	public class EasyUps_Pampers_WebCapture_RL1_728x90 extends CrossDomainCode{
		//this is the 728x90 banner placement 
		public var background:MovieClip;
		public var pottyTrainWith:MovieClip;
		public var pampersBug:MovieClip;
		public var pullupsBackground:MovieClip;
		public var pullupsBug:MovieClip;
		public var betterLeakProtection:MovieClip;
		public var rightToddler:MovieClip;
		public var theClickTag:SimpleButton;
		
		public function EasyUps_Pampers_WebCapture_RL1_728x90() {
			
		}
		
		public function init():void {
			background.alpha = 0;
			pottyTrainWith.alpha = 0;
			pampersBug.alpha = 0;
			pullupsBackground.alpha = 0;
			pullupsBug.alpha = 0;
			betterLeakProtection.alpha = 0;
			rightToddler.alpha = 0;
			theClickTag.addEventListener(MouseEvent.CLICK, doClickTag);
		}
		
		public function startAnimation():void {
			animateBackground(1);
		}
		
		public function animateBackground(fadeAmount:Number = 1):void {
			TweenLite.to(background, .25, {alpha:fadeAmount});
			TweenLite.to(pottyTrainWith, .75, {alpha:1, onComplete:animateInitialSequence});
		}
		
		public function animateInitialSequence():void {
			//start_rL2_babies();
			TweenLite.delayedCall(.25, start_rL2_babies);		//saved 1.5 sec
			//potty train with > pampers & pullups bugs
			TweenLite.to(pottyTrainWith, .75, {alpha:0, delay:1});
			TweenLite.to(pullupsBackground, .5, {alpha:1, delay:1.25});
			TweenLite.to(pullupsBug, .75, {alpha:1, delay:1.25});
			TweenLite.to(pampersBug, .75, {alpha:1, delay:1.25});
		}
		
		public function start_rL2_babies():void {
			myFT.api.richload2.comms.startAnimation();
		}
		
		public function complete_rL1_initialSequence():void {
			trace("initial sequence complete");
			TweenLite.delayedCall(2, animateFinalSequence);
			
		}
		
		public function animateFinalSequence():void {
			TweenLite.to(pullupsBackground, .5, {alpha:0});
			TweenLite.to(pullupsBug, .5, {alpha:0});
			TweenLite.to(betterLeakProtection, .5, {x:0, alpha:1, delay:.5});
			TweenLite.to(rightToddler, .5, {alpha:1, delay:.5});
			TweenLite.delayedCall(.5, contractTheAd);
		}
		
		public function contractTheAd():void {
			trace("should be contracting the ad?");
			myFT.api.expand.contract();
		}
		
		public function doClickTag(evt:MouseEvent):void {
			myFT.clickTag(1);
		}
			

	}
}