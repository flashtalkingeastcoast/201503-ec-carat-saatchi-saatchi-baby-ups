﻿// ---------------- FT CROSS DOMAIN SECURITY CODE -------------------
package com.flashtalking.util
{


	import flash.system.Security;
	import flash.display.MovieClip;
	import flash.events.Event;


	public class CrossDomainCode extends MovieClip
	{
		// This needs to go in the document class which is attached to your additonal load SWF files.
		// It does not need to go in your base adfile.

		// This contains the reference to the ft class so the rich load
		// can listen to and dispatch events
		public var myFT:* = null;
		// this is used as a shim for events targeting, when running this file locally
		// this prevents reference errors when running the rich load as standalone
		public var events:* = new Object  ;
		// document class constructor function, this contains essential checks and security commands
		// for communicating with your base file, please use "constructorFunction()" for your constructor code
		public function CrossDomainCode()
		{
			Security.allowDomain("http://www.flashtalking.net");
			Security.allowDomain("http://a.flashtalking.com");
			Security.allowDomain("http://cdn.flashtalking.com");
			Security.allowDomain("http://video.flashtalking.com");
			Security.allowDomain("*");
			addEventListener(Event.ENTER_FRAME,checkIfBeingLoaded);
		}
		// this function establishes if the rich load is being run via a base file
		// or as a standalone file
		private function checkIfBeingLoaded(evt:Event):void
		{
			// because this is on an enterFrame check myFT should not be null if being run
			// via a base file
			if (myFT == null)
			{
				// the rich load is being run locally, not as part of an add
				// remove the load check
				removeEventListener(Event.ENTER_FRAME,checkIfBeingLoaded);
				// add events object to myFT to prevent reference errors when run locally
				myFT = this;
				events = this;
				// fire the constructor function
				constructorFunction();
			}
		}


		// this function is called from the base file when the loader initialises
		// it passes a reference to the ft class to enable communication
		// it then fires the constructor function, safe in the knowledge that communication
		// with the base file is now possible
		public function ftConnection(ftRef: * ):void
		{
			myFT = ftRef;
			constructorFunction();
		}
		
		
		
		private function constructorFunction():void
		{
			// Call the init function in the richload document class
			MovieClip(root).init();
		}
	}
}
// ---------------- END FT CROSS DOMAIN CODE -------------