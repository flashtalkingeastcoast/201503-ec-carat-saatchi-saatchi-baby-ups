package com.flashtalking.util{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	
	public class WebcaptureParse{
		public var debugWebcaptureParse:Boolean = true;
		
		public var urlLocation:String;
		public var webKey:String;
		public var targetMovieClip:MovieClip;
		public var webcaptureLoaded:Function;
		public var xmlLoaded:Function;
		public var xmlLoadedFunctionDefined:Boolean = false;
		public var defaultWebkeyUrl:String;
		
		public var myXML:XML;
		public var myXML_loader:URLLoader = new URLLoader();
		public var webCapXml:XML;
		public var webcaptureImageURL:String;
		public var xmlLoadFailedReplacementImageURL:String = "";
		
		//public var pageShot:MovieClip;
		public var loader_webcaptureImage:Loader = new Loader();
		public var imageNotLoadedYet:Boolean = true;
		public var loadMatchedImage:Boolean;
		public var shouldLoadToTargetMovieClip:Boolean;
		public var xmlLoadedAndMatchedImage:Boolean = false;
		
		public function WebcaptureParse(theUrlLocation:String, theWebKey:String, theDefaultWebkeyUrl:String) {
			// myVar = newWebcaptureParse("http://cdn.flashtalking.com/25740/assets/specMed.xml",myFT.params["webKey"], "covers.com")
			urlLocation = theUrlLocation;
			webKey = theWebKey;
			defaultWebkeyUrl = theDefaultWebkeyUrl;
			trace2("WebcaptureParse("+urlLocation+", "+webKey+", "+defaultWebkeyUrl+")");
		}
		
		//SETTERS
		public function setTargetMovieClip(theMovieClip:MovieClip):void {
			shouldLoadToTargetMovieClip = true;
			targetMovieClip = theMovieClip;
//test this 
			if(imageNotLoadedYet == false){
				targetMovieClip.addChild(loader_webcaptureImage);
			}
		}
		
		public function setXMLLoadedFunction(theFunction:Function):void {
			xmlLoaded = theFunction;
			xmlLoadedFunctionDefined = true;
		}
		
		public function setWebcaptureLoadedFunction(theFunction:Function):void {
			webcaptureLoaded = theFunction;
		}
		
		public function setXML_loadFailImageURL(theURL:String):void {
			xmlLoadFailedReplacementImageURL = theURL;
		}
		
		// GETTERS
		public function getMatchedImageURL():String {
			return webcaptureImageURL;
		}
		
		public function getLoadedBitmapImageData():Bitmap{
			var duplicationBitmap:Bitmap = new Bitmap(Bitmap(loader_webcaptureImage.content).bitmapData);
			return duplicationBitmap;
		}
		
		/////////////////////////////////////////////////////////////////////////
		public function doXmlLoad(shouldLoadMatchedImage:Boolean = true):void {
			trace2("doing XML load");
			loadMatchedImage = shouldLoadMatchedImage;
			if(imageNotLoadedYet){
				try{
					//load xml
					myXML_loader.load(new URLRequest(urlLocation));
					//parse xml
					myXML_loader.addEventListener(Event.COMPLETE, loadCompleteHandler);
					myXML_loader.addEventListener(IOErrorEvent.IO_ERROR, loadFailedHandler);
				} catch(e:Error){
					loadFailedHandler(null);
				}
			} else {
				webcaptureLoaded();
			}
		}
		
		private function loadCompleteHandler(evt:Event):void{
			myXML_loader.removeEventListener(Event.COMPLETE, loadCompleteHandler);
			myXML_loader.removeEventListener(IOErrorEvent.IO_ERROR, loadFailedHandler);
			
			webCapXml = new XML(myXML_loader.data);
			
			try{
				//check to see if there is a match in the xml
				//Find the node in the XML that contains the "webkey" URL above as the value of node "wsn" and then get the "imgurl" subnode
				webcaptureImageURL = webCapXml.node.(wsn.toLowerCase()==webKey.toLowerCase()).imgurl;
			} catch (e:Error) {
				trace2("loadCompleteHandler -- No matches - loading the default match instead");
				webKey = defaultWebkeyUrl;
				webcaptureImageURL = webCapXml.node.(wsn.toLowerCase()==webKey.toLowerCase()).imgurl;
			}
			
			//if match there and not an empty string then use flashvar else use default site name defined in IA
			if (webcaptureImageURL == "") {
				webKey = defaultWebkeyUrl;
				webcaptureImageURL = webCapXml.node.(wsn.toLowerCase()==webKey.toLowerCase()).imgurl;
			}
			
			//Load the webcap image
			loadWebcapture();
		}
		
		private function loadFailedHandler(evt:Event):void{
			trace2("XML load failed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			myXML_loader.removeEventListener(Event.COMPLETE, loadCompleteHandler);
			myXML_loader.removeEventListener(IOErrorEvent.IO_ERROR, loadFailedHandler);
			//webcaptureImageURL = "http://cdn.flashtalking.com/29494/instantAssets/2_-2.jpg";
			if(xmlLoadFailedReplacementImageURL.length > 1){
				webcaptureImageURL = xmlLoadFailedReplacementImageURL;
				loadWebcapture();
			}
		}
		
		private function loadWebcapture():void {
			xmlLoadedAndMatchedImage = true;
			//test to see if target movie clip is defined
			if(shouldLoadToTargetMovieClip){
				targetMovieClip.addChild(loader_webcaptureImage);
			}
			if(loadMatchedImage){
				loadTheMatchedImage();
			}
			if(xmlLoadedFunctionDefined){
				xmlLoaded();
			}
		}
		
		public function loadTheMatchedImage():void {
			if(xmlLoadedAndMatchedImage){
				loader_webcaptureImage.load(new URLRequest(webcaptureImageURL));
				loader_webcaptureImage.contentLoaderInfo.addEventListener(Event.COMPLETE, assetsLoaded);
			}
		}
		
		private function assetsLoaded(evt:Event):void {
			imageNotLoadedYet = false;
			//trace2(">> loader_webcaptureImage: width : "+loader_webcaptureImage.width);
			//trace2(">> loader_webcaptureImage: height : "+loader_webcaptureImage.height);
			loader_webcaptureImage.contentLoaderInfo.removeEventListener(Event.COMPLETE, assetsLoaded);
			webcaptureLoaded();
		}
		
		private function trace2(msg:String):void {
			if(debugWebcaptureParse){
				trace("[WebcaptureParse] "+msg);
			}
		}
		
	}
}