package com.flashtalking.util
{
	import flash.display.GradientType;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Matrix;
	import flash.text.Font;

	public class DrawingHelper
	{
		
		
		public function DrawingHelper(){
			
		}
		
		public static function convertToFlashHex(val:String):Number {
			var newVal:Number = Number("0x" + val);
			return newVal;
		} 
		
		//create a rectangle with a gradient (top to bottom, linear) in the obj provided in paramaters
		/******************************************
		 * obj - pass it a movie clip to draw a gradient rectangle in
		 * theWidth / theHeight - width and height of the gradient rectangle you want to draw
		 * topColor / bottomColor - the Flash Hex number value of the RBG color you want to draw: e.g. 0xffcc00
		 * topAlpha / bottomAlpha - the alpha numerical value you want to use with the associated color from 0-transparent to 1-opaque
		 * ****************************************/
		
		public static function makeGradient(obj:MovieClip, theWidth:Number, theHeight:Number, topColor:Number = 0xffffff, topAlpha:Number = 1, botomColor:Number = 0xffffff, bottomAlpha:Number = 1, gradientRotation:Number = 90):void {
			var drwobj:Sprite = new Sprite(),
				mat = new Matrix(),
				colors:Array =[topColor,botomColor],
				alphas=[topAlpha,bottomAlpha],
				ratios=[0, 255];
			
			mat.createGradientBox(theWidth,theHeight,toRad(gradientRotation), 0, (theHeight * .15));
			drwobj.graphics.lineStyle();
			drwobj.graphics.beginGradientFill(GradientType.LINEAR,colors,alphas,ratios,mat);
			drwobj.graphics.drawRect(0,0,theWidth,theHeight);
			drwobj.graphics.endFill();
			obj.addChild(drwobj);
		}
		
		public static function toRad(a:Number):Number {
			return a*Math.PI/180;
		}
		
		/*****************************************
		 * Create a customizable color 1 pixel border box for the ad
		 * var theBorder:Shape;
		 * var borderColor:String = "000000";
		 * theBorder = DrawingHelper.drawAdBorder(stage, DrawingHelper.convertToFlashHex(borderColor), 1, 1);
		 * addChild(theBorder);
		 * ***************************************/
		
		public static function drawAdBorder(theStage:Stage, theColor:uint, theWidth:Number, theAlpha:Number):Shape {
			var border:Shape = new Shape();
			border.graphics.beginFill(theColor, theAlpha);
			border.graphics.drawRect(theWidth, 0, theStage.stageWidth-theWidth, theWidth);
			border.graphics.drawRect(theStage.stageWidth-theWidth, theWidth, theWidth, theStage.stageHeight-theWidth);
			border.graphics.drawRect(0, theStage.stageHeight-theWidth, theStage.stageWidth-theWidth, theWidth);
			border.graphics.drawRect(0, 0, theWidth, theStage.stageHeight-theWidth);
			border.graphics.endFill();
			return border;
		}
		
		public static function showEmbeddedFonts ():void {
			trace("========Embedded Fonts========");
			var fonts:Array = Font.enumerateFonts();
			fonts.sortOn("fontName", Array.CASEINSENSITIVE);
			for (var i:int = 0; i < fonts.length; i++) {
				trace(fonts[i].fontName + ", " + fonts[i].fontStyle);
			}
		}
		
	}
}